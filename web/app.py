# Author: Nathan Crawford
#
# implement the same "file checking" logic from Project 1

from flask import Flask, render_template, abort

app = Flask(__name__)

@app.errorhandler(404)
def error_404(e):
    return render_template('404.html'), 404

@app.errorhandler(403)
def error_403(e):
    return render_template('403.html'), 403

@app.route("/<path:name>")
def hello(name):
    if (name.find('~') != -1 or name.find('..') != -1 or name.find('//') != -1):
        abort(403)
    try:
        return render_template(name)
    except:
        abort(404)

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
